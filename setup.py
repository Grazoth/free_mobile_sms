#!/usr/bin/env python3
""" Setup.py du programme free_mobile_sms."""

import os
from setuptools import setup

try:
    # get version from git describe
    VERSION = os.popen('git describe --tags').read()[:-1]
except OSError:
    pass

setup(
    name='free_mobile_sms',
    version=VERSION,
    author='Grazoth',
    author_email='grazoth chez zaclys point net',
    packages=['free_mobile_sms'],
    url='https://framagit.org/Grazoth/free_mobile_sms',
    license=open('LICENSE').read(),
    description='Python script for use FreeMobile SMS api',
    long_description=open('README.md').read(),
    python_requires='>=3.6',
    install_requires=[],
    entry_points = {
        'console_scripts': [
            'sendsms = free_mobile_sms.sendsms:main',
        ],
    },
    )
