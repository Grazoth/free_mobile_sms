#!/usr/bin/python3
""" Module de test du module sendsms.

Ce module utilise unittest, ce lance simplement avec
    python3 test_sendsms.py.
"""
# pylint: disable=protected-access
# pylint: disable=too-few-public-methods
import unittest
import os
import urllib.request

import sendsms

TEMP_DIR = '/tmp'
class FileCheckerTestCase(unittest.TestCase):
    """ Teste la classe FileChecker."""
    TEST_PATH = TEMP_DIR + '/delete_me'

    def _create_FileChecker_instance(self):# pylint: disable=invalid-name
        return sendsms.FileChecker(self.TEST_PATH)

    def _create_file(self):
        try:
            os.system('touch ' + self.TEST_PATH)
            os.chmod(self.TEST_PATH, 0o770)

        except:
            raise IOError('Impossible de créer le fichier de test')

    def test_is_exist(self):
        """ Teste la méthode is_exist."""
        i = self._create_FileChecker_instance()

        self._create_file()
        i.is_exist()
        os.remove(i.path)

        try:
            i.is_exist()

        except FileNotFoundError as err:
            self.assertEqual(err.args[0], (f"Le fichier {self.TEST_PATH}"
                                           " n'existe pas"))

    def test_is_file(self):
        """ Teste la méthode is_file."""
        i = self._create_FileChecker_instance()

        self._create_file()
        i.is_file()
        os.remove(i.path)

        try:
            i.is_file()

        except IsADirectoryError as err:
            self.assertEqual(err.args[0], (f"{self.TEST_PATH}"
                                           " n'est pas un fichier"))

    def test_is_readable(self):
        """ Teste la méthode is_readable."""
        i = self._create_FileChecker_instance()

        self._create_file()
        i.is_readable()
        os.remove(i.path)

        try:
            i.is_readable()

        except IOError as err:
            err_msg = ("Vous n'avez pas les droits en lecture sur le fichier"
                       f" {self.TEST_PATH}")
            self.assertEqual(err.args[0], err_msg)

    def test_is_writable(self):
        """ Teste la méthode is_writable."""
        i = self._create_FileChecker_instance()

        self._create_file()
        i.is_writable()
        os.remove(i.path)

        try:
            i.is_writable()

        except IOError as err:
            err_msg = ("Vous n'avez pas les droits en écriture sur le fichier"
                       f" {self.TEST_PATH}")
            self.assertEqual(err.args[0], err_msg)

    def test_is_OK(self):# pylint: disable=C0103
        """ Teste la méthode is_OK."""
        i = self._create_FileChecker_instance()
        self._create_file()
        i.is_OK()
        os.remove(i.path)


class ConfigTestCase(unittest.TestCase):
    """ Teste la classe Config."""
    __config_path = 'sendsmsrc'
    __account = 'account'

    def test_get_user(self):
        """ Teste la méthode get_user."""
        i = sendsms.Config(self.__config_path, self.__account)
        self.assertEqual(i.get_user(), 'VotreNumeroDeClient')

    def test_get_password(self):
        """ Teste la méthode get_password."""
        i = sendsms.Config(self.__config_path, self.__account)
        self.assertEqual(i.get_password(), 'VotreMotDpasse')


class SmsTestCase(unittest.TestCase):
    """ Test la classe Sms."""
    __message = 'salut les musclés & à +'
    good_url = ('https://smsapi.free-mobile.fr/sendmsg?'
                'user=VotreNumeroDeClient&'
                'pass=VotreMotDpasse&'
                'msg=salut+les+muscl%C3%A9s+%26+%C3%A0+%2B')

    def test___init__(self):
        """ Teste la méthode __init__."""
        arg = 'toto'

        empty_sms = sendsms.Sms()
        self.assertEqual(empty_sms.message, '')

        one_arg_sms = sendsms.Sms(arg)
        self.assertEqual(one_arg_sms.message, arg)

        multi_arg_sms = sendsms.Sms(arg, arg)
        self.assertEqual(multi_arg_sms.message, arg + ' ' + arg)

    def _create_Sms_instance(self, *message, config_file='sendsmsrc'):
        """ Renvoie une instance de Sms."""
        # pylint: disable=invalid-name
        # pylint: disable=no-self-use
        return sendsms.Sms(' '.join(message), config_file=config_file)

    def test_get_url(self):
        """ Teste la méthode get_url."""
        i = self._create_Sms_instance(self.__message)

        url = i.get_url()
        self.assertEqual(url, self.good_url)

    def test_send(self):
        """ Teste la méthode send."""

        class FakeRequest():
            """ Cette classe est destiné à remplacer urllib.request.

            Fourni les methodes getcode et urlopen ainsi que les exceptions
            HTTPError et URLError.
            """

            # pylint: disable=missing-docstring
            class HTTPError(urllib.request.HTTPError):
                pass

            class URLError(urllib.request.URLError):
                pass

            class ReturnCode():
                """ À chaque appel d’une fonction décoré renvoi un code différent.

                À chaque fois que la fonction décoré par cette classe est appelé,
                renvoie un des codes erreur contenu dans sendsms.ERROR_CODES.keys()
                renvoi le code 900 (code inconnue) en dernier.
                """

                def __init__(self, function):
                    self.codes = [k for k in sendsms.ERROR_CODES]
                    self.codes.extend([900, 200])
                    self.function = function

                def __call__(self, url):
                    try:
                        return self.function(url, self.codes.pop(0))
                    except IndexError:
                        raise urllib.request.URLError('bad url')

            class Request():
                def __init__(self):
                    pass

                def getcode():# pylint: disable=no-method-argument
                              # pylint: disable=no-self-use
                    return 200

            def __init__(self):
                pass

            @ReturnCode
            def urlopen(url, code): # pylint: disable=no-self-argument
                """ Lève une exception avec le code fourni en paramètre."""
                if code != 200:
                    raise urllib.request.HTTPError(url, code, None, None, None)
                else:
                    self.assertEqual(url, self.good_url)
                    return FakeRequest.Request


        # Remplace urllib.request.urlopen
        original_request = sendsms.urllib.request
        sendsms.urllib.request = FakeRequest

        # Teste tout les codes erreurs
        continu = True
        while continu:
            i = self._create_Sms_instance(self.__message)
            try:
                i.send()
                self.assertEqual(i.request.getcode(), 200)

            except urllib.request.HTTPError as err:
                if err.code not in sendsms.ERROR_CODES.keys():
                    err_msg = f'{i.get_url()}\nCode d’erreur inconnu err.code'
                    self.assertEqual(err.msg, err_msg)

                elif err.msg != (f'{i.get_url()}\n'
                                 f'{sendsms.ERROR_CODES[err.code]}'):
                    raise urllib.request.HTTPError(i.get_url,
                                                   err.code,
                                                   err.msg,
                                                   err.hdrs,
                                                   err.fp)

            except urllib.request.URLError as err:
                reason = (f'Erreur dans l\'url "{sendsms.HOST}".'
                          f' Cette dernière pourrait avoir changé.'
                          ' <urlopen error bad url>')
                self.assertEqual(err.reason, reason)
                continu = False

        sendsms.urllib.request = original_request


if __name__ == '__main__':
    unittest.main()

# vim:foldmethod=indent
