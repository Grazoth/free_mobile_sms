#!/usr/bin/python3
""" Module servant à l'envoie de sms via l'api sms de freemobile

Ce module peut être utilisé en tant que script isolé de façon simple
    python3 sendsms.py '{texte}'
"""

import sys
import os
import configparser
import urllib.request

HOST = 'https://smsapi.free-mobile.fr/sendmsg'
CONFIG_FILENAME = '.sendsmsrc'
CONFIG_PATH = os.getenv('HOME') + '/' + CONFIG_FILENAME
DEFAULT_ACCOUNT = 'account'
ARG_SEPARATOR = ' '

ERROR_CODES = {400: "Paramètre manquant.",
               402: "Trop de SMS ont été envoyés en trop peu de temps.",
               403: ("Le service n'est pas activé sur l'espace abonné, ou"
                     " login / clé incorrect."),
               500: "Erreur côté serveur. Veuillez réessayer ultérieurement."}

class FileChecker():
    """ Check file existance, type and rights of file.

    if mode = 'r' the file is OK if is readable.
    if mode = 'w' the file is OK if is writable.

    Need module os to work
    """

    def __init__(self, path, mode='rw'):
        self.path = path
        self.mode = mode

    def is_exist(self):
        """ Raise FileNotFoundError if file not exist"""
        if not os.access(self.path, os.F_OK):
            error_message = "Le fichier {} n'existe pas"
            raise FileNotFoundError(error_message.format(self.path))

    def is_file(self):
        """ Raise IsADirectoryError if file is a directory"""
        if not os.path.isfile(self.path):
            error_message = "{} n'est pas un fichier"
            raise IsADirectoryError(error_message.format(self.path))

    def is_readable(self):
        """ Raise IOError if file is not readable"""
        if not os.access(self.path, os.R_OK):
            error_message = "Vous n'avez pas les droits en lecture sur le fichier {}"
            raise IOError(error_message.format(self.path))

    def is_writable(self):
        """ Raise IOError if file is not writable"""
        if not os.access(self.path, os.W_OK):
            error_message = "Vous n'avez pas les droits en écriture sur le fichier {}"
            raise IOError(error_message.format(self.path))

    def is_OK(self):# pylint: disable=C0103
        """ Raise error if file is not ok for operate"""
        self.is_exist()
        self.is_file()
        if self.mode.find('r') != -1:
            self.is_readable()

        if self.mode.find('w') != -1:
            self.is_writable()


class Config():
    """ Fournit le nom d'utilisateur et le mot de passe.

    Récupère le nom d'utilisateur et le mot de passe contenus dans le
    fichier de configuration.

    Utiliser get_user() et get_password() pour récupérer les informations.
    """

    def __init__(self, path=CONFIG_PATH, account=DEFAULT_ACCOUNT):
        config_file = FileChecker(path, 'r')
        config_file.is_OK()

        self.account = account

        self.config = configparser.ConfigParser()
        self.config.read(path)

    def get_user(self):
        """ Renvoie le user."""
        return self.config.get(self.account, 'user')

    def get_password(self):
        """ Renvoie le password."""
        return self.config.get(self.account, 'password')


class Sms():
    """ Encapsule les méthodes nécessaire à l'envoie de sms.

    Créer une instance avec instance = Sms(message)

    La methode send() permet d'envoyer le sms, le code de retour de la
    requette http est récupérable grace à instance.request.getcode().
    """

    def __init__(self, *sms_message, separator=' ', config_file=CONFIG_PATH):
        # sans replace() les retours à la ligne sont affichés '\n' dans le sms.
        self.message = separator.join(sms_message).replace('\\n', '\n')

        self.request = ''

        self.cfg = Config(path=config_file)

    def get_url(self):
        """ Renvoie l’url tel qu'elle sera utilisé dans la requête HTTP."""
        params = {'user': self.cfg.get_user(),
                  'pass': self.cfg.get_password(),
                  'msg': self.message}
        return f'{HOST}?{urllib.parse.urlencode(params)}'

    def send(self):
        """ Envoie le sms.

        Invoquer simplement instance.send().
        """
        try:
            self.request = urllib.request.urlopen(self.get_url())

        except urllib.request.HTTPError as err:
            if err.code in ERROR_CODES.keys():
                error_msg = f'{self.get_url()}\n{ERROR_CODES[err.code]}'
            else:
                error_msg = f'{self.get_url()}\nCode d’erreur inconnu err.code'

            raise urllib.request.HTTPError(self.get_url,
                                           err.code,
                                           error_msg,
                                           err.hdrs,
                                           err.fp)

        except urllib.request.URLError as err:
            error_message = (f'Erreur dans l\'url "{HOST}".'
                             f' Cette dernière pourrait avoir changé. {err}')
            raise urllib.request.URLError(error_message)


def main():
    """ Envoie par SMS le texte passé en argument au script."""
    message = ARG_SEPARATOR.join(sys.argv[1:])
    sms = Sms(message)

    sms.send()
    print('return code', sms.request.getcode())


if __name__ == '__main__':
    main()

# vim:foldmethod=indent
