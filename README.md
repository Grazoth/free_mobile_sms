# Présentation

Free mobile sms est une application python permettant d'envoyer des sms via
l'api sms de free-mobile en ligne de commande [Plus d'explications
ici](https://www.freenews.fr/freenews-edition-nationale-299/free-mobile-170/nouvelle-option-notifications-par-sms-chez-free-mobile-14817).


L'application n'est pour l'instant compatible qu'avec un environnement
linux/unix


# Utilisation

Pour fonctionner, l'application a besoin de votre identifiant et de votre mot
de passe, enregistrez les dans le fichier **~/.sendsmsrc** à l'aide du fichier
**sendsmsrc** présent dans le dépot.


Une fois le fichier de configuration correctement créé, vous pouvez vous
envoyer des sms simplement en appelant le script `./sendsms.py le texte de
votre sms`.
